const fs = require('fs')
// reading data writing data
// use FS module
// const textIn = fs.readFileSync('./txt/input.txt', 'utf-8')
// // synchronous version 
// console.log(textIn)
// const textOut = `What we know about the Avocado:${textIn}.\n Created on ${Date.now()}`// console.log(textOut)
// // writing 
// fs.writeFileSync('./txt/output.txt', textOut)

// synchronous code  block code

//asynchornous non blocking
fs.readFile('./txt/start.txt', 'utf-8', (err, data1) => {
  if (err) {
    return console.log('Error')

  }
  fs.readFile(`./txt/${data1}.txt`, 'utf-8', (err, data2) => {
    console.log(data2)
    fs.readFile('./txt/append.txt', 'utf-8', (err, data3) => {
      console.log(data3)
      fs.writeFile('./txt/final.txt', `${data2}\n${data3}`, 'utf-8', err => {
        console.log('Your file has been written')

      })
    })
  })
})

console.log('Will read file!')
