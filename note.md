# Node.js 就是运行在服务端的 JavaScript
## Node.js REPL(Read Eval Print Loop:交互式解释器) 
```js
➜  1-node-farm git:(main) node 
>Welcome to Node.js v19.0.0.
Type ".help" for more information.
> name
Uncaught ReferenceError: name is not defined
> const name = "baby"
undefined
> name
'baby'
> .exit
```
or use <kbd>Control+D</kbd>
double <kbd>Tab</kbd>
```js
➜  1-node-farm git:(main) ✗ node
       Welcome to Node.js v19.0.0.
Type ".help" for more information.
> 
AbortController                   AbortSignal                       AggregateError                    Array
ArrayBuffer                       Atomics                           BigInt                            BigInt64Array
BigUint64Array                    Blob                              Boolean                           BroadcastChannel

<!--Constructors  Node modules -->
...
> 3*7*8
168
> _+2
170
> 
underscore is previous result
> String.
String.__proto__             String.hasOwnProperty        String.isPrototypeOf         String.propertyIsEnumerable  String.toLocaleString
String.valueOf

String.apply                 String.arguments             String.bind                  String.call                  String.caller
String.constructor  ....
```